const mongoose = require('mongoose');
const { Schema } = mongoose;

const thingsSchema = new Schema({
  user_id: Number,
  title: String,

});

mongoose.model('Things', thingsSchema);
