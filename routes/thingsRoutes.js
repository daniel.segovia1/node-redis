const mongoose = require('mongoose');
const Things = mongoose.model('Things');

const redis = require("redis");
const client = redis.createClient();

module.exports = app => {
  
  app.get('/things', async (req, res) => {
    console.log("----------- find all -------------")

    try {
      const things = await Things.find();
      res.status(200).send(things);
    } catch (err) {
      res.status(400).send(err);
    }
    
  });

  app.post('/things', async (req, res) => {
    console.log("----------- insert thing -------------")
    const { title, user_id } = req.body;

    const thing = new Things({
      title, user_id
    });

    try {
      await thing.save();
      res.status(200).send(thing);
      client.del(user_id);
    } catch (err) {
      res.status(400).send(err);
    }

  });


  app.get('/things/find/:user_id', async (req, res) => {
    console.log("----------- find by user -------------")
    const { user_id } = req.params;
    try {
      const things = await Things
        .find({"user_id": user_id})
        .cache(user_id)

      res.status(200).send(things);
    } catch (err) {
      console.log(err)
      res.status(400).send(err);
    }
    
  });
  
};
