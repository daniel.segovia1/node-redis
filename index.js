const express = require('express');
const cache = require('./services/cache');
const mongoose = require('mongoose');
const keys = require('./config/keys');



require('./models/Things');

mongoose.Promise = global.Promise;
mongoose.connect(keys.mongoURI, { useMongoClient: true });

const app = express();

app.use(express.json());
require('./routes/thingsRoutes')(app);

app.get('/', async (req, res) => {
  res.send("hola mundo");
});

const PORT = process.env.PORT || 5112;

app.listen(PORT, () => {
  console.log(`Listening on port: `, PORT);
});
