const mongoose = require('mongoose');

const redis = require("redis");
const client = redis.createClient();
const util = require('util')
client.hget = util.promisify(client.hget)

const exec = mongoose.Query.prototype.exec;

mongoose.Query.prototype.cache = async function (group) {
    console.log("fuerzo uso de cache")
    this.useCache = true
    this.groupCache = group || "generic"
    return this
}

mongoose.Query.prototype.exec = async function () {

    if(!this.useCache){
        console.log("retorno desde mongo sin utilizar logica de cache")
        return await exec.apply(this, arguments)
    }

    console.log("comienza el uso de la logica de cache")
    
    const key = this.mongooseCollection.name + JSON.stringify(this.getQuery())
    console.log("group: " + this.groupCache + " key: " + key)

    let data = await client.hget(this.groupCache, key);
    if(data){
        console.log("retorno de cache")
        return JSON.parse(data);
    }

    console.log("retorno de mongo")
    const result = await exec.apply(this, arguments)
    client.hset(this.groupCache, key, JSON.stringify(result), 'EX', 60);
    return result;
}

